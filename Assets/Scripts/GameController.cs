﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class GameController : MonoBehaviour {

	public static GameController Instance;


	public Button start;
	public int difficultySelect;
	public GameObject[] difficultyButtons;

	private GameObject loadScreen;
	public GameObject loadDifficultyScreen;


	void Start () 
	{

	}
	
	void Update () 
	{
	
	}

	public void loadMainScreen()
	{
		loadScreen = GameObject.Find ("LoadScreen");
		loadScreen.SetActive(true);
	}

	public void startPress()
	{
		loadScreen = GameObject.Find("StartScreen");
		loadScreen.SetActive(false);
	}

	public void loadDifficulties() 
	{
		loadDifficultyScreen = GameObject.Find ("DifficultySelection");
		loadDifficultyScreen.SetActive (true);
	}

	public void easyPress()
	{
		loadDifficultyScreen = GameObject.Find("DifficultySelection");
		loadDifficultyScreen.SetActive (false);	
	}

	public void mediumPress()
	{
		loadDifficultyScreen = GameObject.Find("DifficultySelection");
		loadDifficultyScreen.SetActive (false);
		Application.LoadLevel("Scene1");
	}
		

	public void hardPress()
	{
		loadDifficultyScreen = GameObject.Find ("DifficultySelection");
		loadDifficultyScreen.SetActive (false);
		Application.LoadLevel ("Scene3");
	}
}




