using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Player : MonoBehaviour {

	private Animator animator;
	private GameObject player;
	private int playerHealth = 50;
	private int healthPerCoin = 10;
	private int healthPerEnemyHit = 5;

	private GameObject gameOver1;
	public Text healthText;
	public float moveSpeed;
	public AudioClip attackSound1;
	public GameObject loadDifficultyScreen;
	public Button gameOver;

	void Start () {
		animator = GetComponent<Animator> ();
		player = GameObject.Find ("Player");
		healthText.text = "Health: " + playerHealth;
	}

		void Update () {

		GameOver ();

		if(Input.GetKey(KeyCode.UpArrow))
		{
		transform.position += Vector3.up * moveSpeed * Time.deltaTime;			
			animator.SetTrigger ("archerJump");
	    //make jump animation trigger and amount of movement for that jump 
		}
		
		if(Input.GetKey(KeyCode.DownArrow))
		{
			transform.position += Vector3.down * moveSpeed * Time.deltaTime;			
			/*Vector3 position = this.transform.position;
			position.y--;
			this.transform.position = position;*/
		}

		if (Input.GetKey (KeyCode.LeftArrow)) 
		{
		animator.SetTrigger ("archerWalk");
		transform.position += Vector3.left * moveSpeed * Time.deltaTime;
		transform.rotation = Quaternion.Euler(new Vector3(0, 180, 0f));
			//if(Input.GetKey (KeyCode.LeftArrow != null)){
				//set archerwalk equal to false
			//}
		}

		if (Input.GetKey (KeyCode.RightArrow)) 
		{
		animator.SetTrigger ("archerWalk");
		transform.position += Vector3.right * moveSpeed * Time.deltaTime;
		transform.rotation = Quaternion.Euler(new Vector3(0, 0, 0f));

		}
	}

	private void OnTriggerEnter2D(Collider2D objectPlayerCollidedWith) 
	{
		if (objectPlayerCollidedWith.tag == "Floors") {
			animator.SetTrigger("archerHit");
			playerHealth -= healthPerEnemyHit;
			healthText.text = "-" + healthPerEnemyHit + " Health\n" + "Health: " + playerHealth;
		} else if (objectPlayerCollidedWith.tag == "Enemy") {
			animator.SetTrigger ("archerHit");
			playerHealth -= healthPerEnemyHit;
			healthText.text = "-" + healthPerEnemyHit + " Health\n" + "Health: " + playerHealth;
		} else if (objectPlayerCollidedWith.tag == "Item") {
			playerHealth += healthPerCoin;
			healthText.text = "+" + healthPerCoin + " Health\n" + "Health: " + playerHealth;
			objectPlayerCollidedWith.gameObject.SetActive (false);
		}
	}

	public void TakeDamage(int damageReceived) 
	{
		playerHealth -= damageReceived;
		//GameController.Instance.healthText.text = "-" + damageReceived + " Health\n" + "Health: " + playerHealth;
		animator.SetTrigger ("archerHit");
		Debug.Log (playerHealth);
	}

	public void GameOver()
	{
		if (playerHealth <= 0) 
		{
			Application.LoadLevel (1);
			//loadDifficultyScreen.GetComponent<GameController>().loadDifficulties();		
			//GameController difficultySelect = loadDifficultyScreen.GetComponent<GameController>();
		}

	}

	/*public void restartPress() 
	{
		Application.LoadLevel (1);
	}*/

}