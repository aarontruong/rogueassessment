using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour {

	private Animator animator;
	private Transform player;

	public int attackDamage;
	public float moveSpeed;
	public AudioClip attackSound1;


	void Start () {
		animator = GetComponent<Animator> ();
		player = GameObject.FindGameObjectWithTag ("Characters").transform;
	}


	void Update () 
	{
		MoveEnemy ();
	} 


	private void OnTriggerEnter2D(Collider2D objectPlayerCollidedWith) 
	{
		if (objectPlayerCollidedWith.tag == "Characters") {
			animator.SetTrigger("enemyAttack");
			//SoundController.Instance.PlaySingle(attackSound1);
		}
	}

	public void MoveEnemy(){

	int xAxis = 0;
	int yAxis = 0;
	
	float xAxisDistance = Mathf.Abs (player.position.x - transform.position.x);
	float yAxisDistance = Mathf.Abs (player.position.y - transform.position.y);
	
	if(xAxisDistance > yAxisDistance)
	{
		if (player.position.x > transform.position.x) 
		{
			transform.Translate(.1f, 0, 0);
			//transform.rotation = Quaternion.Euler(new Vector3(0, 0, 0f));
			} 
		
		else 
		{
			transform.Translate(-.1f, 0, 0);
			//transform.rotation = Quaternion.Euler(new Vector3(0, 180, 0f));

		}
			transform.position += Vector3.left * moveSpeed * Time.deltaTime;
			animator.SetTrigger ("enemyWalk");

	} 

	else 
	{
		if(player.position.y > transform.position.y)
		{
			transform.Translate (0, .1f, 0);
		}
		
		else
		{
			transform.Translate (0, -.1f, 0);
		}
			transform.position += Vector3.right * moveSpeed * Time.deltaTime;
			animator.SetTrigger ("enemyJump");
		}
	}

	private void HandleCollision<T>(T component) {
		Player player = component as Player;
		player.TakeDamage (attackDamage);
		Debug.Log (attackDamage + "hello");
	}
}